//
//  aeroWXApp.swift
//  aeroWX
//
//  Created by Pavel Belov on 02.02.2021.
//

import SwiftUI
import Firebase

@main
struct aeroWXApp: App {
    let persistenceController = PersistenceController.shared
    
    init() {
        FirebaseApp.configure()
    }

    var body: some Scene {
        WindowGroup {
            MainView()
            .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
