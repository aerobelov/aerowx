

import Foundation
import Combine

struct SearchManager {
    
    func buildRequest(arp: String) -> URLRequest? {
        
            let arpurl: URL
            let urlConstructor = UrlConstructor.station(arp)
            let succ_url = urlConstructor.urls([])
            switch succ_url {
                case .success(let url):
                    arpurl = url
                    let request = RequestConstructor.returnBaseRequest(arpurl, method: .get, header: .avwx)
                    //print(request)
                    return  request
                case .failure(_):
                    return nil
            }
    }
    
    
    func searchPublisher(request: URLRequest) -> AnyPublisher<Station, Never> {
     
        //URLSESSION PUBLISHER
        return URLSession.shared
            .dataTaskPublisher(for: request)
            .tryMap { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    //print("RESPONSE\(element.response)")
                    throw UrlError.invalidResponse
                }
                return element.data
            }
            .decode(type: Station.self, decoder: JSONDecoder())
            .replaceError(with: Station())
            .eraseToAnyPublisher()
       
    }
 
}
