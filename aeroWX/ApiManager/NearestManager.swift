//
//  AvwxNearestManager.swift
//  aeroWX
//
//  Created by Pavel Belov on 18.02.2021.
//

import Foundation
import Combine

struct NearestManager {
    
    func buildRequest(crd: (Double, Double)) -> URLRequest? {
        
        var wxurl: URL
        let urlConstructor = UrlConstructor.stations(crd)
        let n = URLQueryItem(name: "n", value: "5")
        let airport = URLQueryItem(name: "airport", value: "true")
        let reporting = URLQueryItem(name: "reporting", value: "true")
        let format = URLQueryItem(name: "format", value: "json")
        let succ_url = urlConstructor.urls([n, airport, reporting, format])
        switch succ_url {
            case .success(let url):
                wxurl = url
                let request = RequestConstructor.returnBaseRequest(wxurl, method: .get, header: .avwx)
                return  request
            case .failure(_):
                return nil
        }
    }
    
    func nearestPublisher(request: URLRequest) -> AnyPublisher<[StationData], UrlError> {
     
        //URLSESSION PUBLISHER
        return URLSession.shared
            .dataTaskPublisher(for: request)
            .tryMap { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                   // print("RESPONSE ERROR \(element.response)")
                    throw UrlError.invalidResponse
                }
                //print(String(bytes: element.data, encoding: .utf8))
                return element.data
            }
            .decode(type: [StationData].self, decoder: JSONDecoder())
            .map { $0.filter { airport in
                return airport.station.icao != ""
                }
            }
            .mapError { error in
                print(error)
                return UrlError.invalidURL }
            .eraseToAnyPublisher()
    }
  
}

//https://avwx.rest/api/station/near/coord?n=3&airport=true&reporting=true&format=json
