//
//  MetarManager.swift
//  aeroWX
//
//  Created by Pavel Belov on 19.02.2021.
//

import Foundation
import Combine

class MetarManager: ObservableObject {
    
    func buildRequest(icao: String) -> URLRequest? {
        
        var wxurl: URL
        let urlConstructor = UrlConstructor.metar(icao)
        let format = URLQueryItem(name: "format", value: "json")
        let succ_url = urlConstructor.urls([format])
        switch succ_url {
            case .success(let url):
                wxurl = url
                let request = RequestConstructor.returnBaseRequest(wxurl, method: .get, header: .avwx)
                return  request
            case .failure(_):
                return nil
        }
    }
    
    func metarPublisher(request: URLRequest) -> AnyPublisher<MetarData, Never> {
     
        //URLSESSION PUBLISHER
        return URLSession.shared
            .dataTaskPublisher(for: request)
            .tryMap { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    //print("METAR_RESPONSE\(element.response)")
                    throw UrlError.invalidResponse
                }
                return element.data
            }
            .decode(type: MetarData.self, decoder: JSONDecoder())
            .replaceError(with: MetarData())
            .eraseToAnyPublisher()
       
    }
    
}
