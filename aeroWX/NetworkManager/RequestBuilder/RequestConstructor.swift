//
//  RequestConstructor.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

class RequestConstructor {
    
    static func returnBaseRequest( _ url: URL, method: RequestMethod, header: RequestHeaders) -> URLRequest? {
        
        var request: URLRequest?
        
        request = URLRequest(url: url)
        request?.httpMethod = method.rawValue
        request?.allHTTPHeaderFields = header.getHeader()
        
        guard request != nil else { return nil }
        return request
    }
}
