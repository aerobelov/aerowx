//
//  Headers.swift
//  aeroWX
//
//  Created by Pavel Belov on 08.02.2021.
//

import Foundation

enum RequestHeaders {
    
    case avwx
    
    func getHeader() -> [String:String] {
        switch self {
        case .avwx:
            return [
                "Authorization": "abG_sFedeIYCxl8AK0tZNNUYRjQtOaXVuKxyAd2dphw"
            ]
        }
    }
}
