//
//  UrlHost.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlHost: String {
    case rapidApiNearest = "aviation-reference-data.p.rapidapi.com" //Nearest airports
}
