//
//  URLConstructor.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlConstructor  {
    
    case nearest
    
    func urls(_ query: [URLQueryItem] = []) -> Result<URL, UrlError> {
        switch self {
        case .nearest:
            var urlComponent = URLComponents()
            urlComponent.scheme = UrlScheme.http.rawValue
            urlComponent.host = UrlHost.rapidApiNearest.rawValue
            urlComponent.path = UrlPath.nearestPath.rawValue
            urlComponent.queryItems = query
            guard urlComponent.url != nil else { return .failure(.invalidURL) }
            return .success(urlComponent.url!)
        }
    }
}
