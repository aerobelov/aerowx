//
//  UrlPath.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlPath: String {
    case avwxNearest = "/api/station/near/"
    case station = "/api/station/"
    case metar = "/api/metar/"
    case taf = "/api/taf/"
}
