//
//  URLConstructor.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlConstructor  {
    
    case station(String)
    case stations((Double, Double))
    case metar(String)
    case taf(String)
    
    func urls(_ query: [URLQueryItem] = []) -> Result<URL, UrlError> {
        switch self {
            case .stations(let crd):
                var urlComponent = URLComponents()
                urlComponent.scheme = UrlScheme.https.rawValue
                urlComponent.host = UrlHost.avwx.rawValue
                urlComponent.path = UrlPath.avwxNearest.rawValue
                urlComponent.queryItems = query
                guard urlComponent.url != nil else {
                    return .failure(.invalidURLCreation)
                }
                let result = urlComponent.url!.appendingPathComponent(String("\(crd.0),\(crd.1)"))
                return .success(result)
                
            case .metar(let icao):
                var urlComponent = URLComponents()
                urlComponent.scheme = UrlScheme.https.rawValue
                urlComponent.host = UrlHost.avwx.rawValue
                urlComponent.path = UrlPath.metar.rawValue
                urlComponent.queryItems = query
                guard urlComponent.url != nil else {
                    return .failure(.invalidURLCreation)
                }
                let result = urlComponent.url!.appendingPathComponent(icao)
                return .success(result)
                
            case .taf(let icao):
                var urlComponent = URLComponents()
                urlComponent.scheme = UrlScheme.https.rawValue
                urlComponent.host = UrlHost.avwx.rawValue
                urlComponent.path = UrlPath.taf.rawValue
                urlComponent.queryItems = query
                guard urlComponent.url != nil else {
                    return .failure(.invalidURLCreation)
                }
                let result = urlComponent.url!.appendingPathComponent(icao)
                return .success(result)
           
            case .station(let code):
                var urlComponent = URLComponents()
                urlComponent.scheme = UrlScheme.https.rawValue
                urlComponent.host = UrlHost.avwx.rawValue
                urlComponent.path = UrlPath.station.rawValue
                urlComponent.queryItems = query
                guard urlComponent.url != nil else {
                    return .failure(.invalidURLCreation)
                }
                var tempurl = urlComponent.url
                tempurl!.appendPathComponent(code, isDirectory: false)
                return .success(tempurl!)
        }
    }
    
}
