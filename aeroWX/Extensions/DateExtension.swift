//
//  DateExtension.swift
//  aeroWX
//
//  Created by Pavel Belov on 26.02.2021.
//

import Foundation

extension Date {
    func short() -> String {
        let metarDateFormatter = DateFormatter()
        metarDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        metarDateFormatter.dateFormat = "dd.MM.yy HH:mm"
        let result = "\(metarDateFormatter.string(from: self)) Z"
        return result
    }
}
