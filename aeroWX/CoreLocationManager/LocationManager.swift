//
//  LocationManager.swift
//  aeroWX
//
//  Created by Pavel Belov on 08.02.2021.
//

import Foundation
import CoreLocation
import Combine

class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    var manager = CLLocationManager()
    //var crd = CurrentValueSubject<(Double, Double), Never>((0,0))
    var crd2D = CurrentValueSubject<CLLocationCoordinate2D, Never>(CLLocationCoordinate2D())
    @Published var coord: (Double, Double) = (0,0)
    @Published var coord2d: CLLocationCoordinate2D = CLLocationCoordinate2D()
    @Published var errorMessage: String?
    
    override init() {
        super.init()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.distanceFilter = 1000
        manager.desiredAccuracy = kCLLocationAccuracyReduced
        manager.startUpdatingLocation()
        update()
    }
    
    func update()  {
        if manager.authorizationStatus == .authorizedAlways || manager.authorizationStatus == .authorizedWhenInUse {
            if let currentLoc = manager.location {
                //self.coord = (currentLoc.coordinate.latitude, currentLoc.coordinate.longitude)
                //self.crd.send((currentLoc.coordinate.latitude, currentLoc.coordinate.longitude))
                self.crd2D.send(currentLoc.coordinate)
            }
        } else {
            manager.stopUpdatingLocation()
            //self.crd.send((0, 0))
            self.crd2D.send(CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
        self.crd2D.send(CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
        self.errorMessage = error.localizedDescription
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLoc = locations.last {
            self.crd2D.send(currentLoc.coordinate)
           // self.coord = (currentLoc.coordinate.latitude, currentLoc.coordinate.longitude)
        }
    }
   
}
