//
//  SearchViewModel.swift
//  aeroWX
//
//  Created by Pavel Belov on 02.02.2021.
//

import Foundation
import Combine
import SwiftUI
import MapKit

class NearestViewModel: ObservableObject {
    
    @Published var airports: [StationIntegratedData]
    var placeMarkers: [nearestPlaceMarker]
    @Published var coordinate: CLLocationCoordinate2D?
    @Published var span: (Double, Double) = (0.2, 0.2)
    @Published var region = MKCoordinateRegion (
        center: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0),
        span: MKCoordinateSpan(latitudeDelta: 0.7, longitudeDelta: 0.7)
    )
    @ObservedObject var locationManager: LocationManager
    var disposable = Set<AnyCancellable>()
    var coordinateSubscription: AnyCancellable?
    var apiSubscription: AnyCancellable?
    var markersSubscription : AnyCancellable?
    
    init() {

        self.airports = []
        self.locationManager = LocationManager()
        self.placeMarkers = []//airports.map { $0.marker }
        coordinateSubscription =
        locationManager.crd2D
            .receive(on: RunLoop.main)
            .sink { [weak self] (coordinate) in
                self?.coordinate = coordinate
                self?.region.center = coordinate
                self?.updateList(coord: coordinate)
            }
        markersSubscription =
            self.$airports
            .receive(on: RunLoop.main)
            .sink {_ in
                self.placeMarkers = self.createMarkers(self.airports)
            }
    }

    func updateList(coord: CLLocationCoordinate2D) {
        let apiManager = NearestManager()
        
        if let request = apiManager.buildRequest(crd: (coord.latitude, coord.longitude)) {
            let updatePublisher = apiManager.nearestPublisher(request: request)
            apiSubscription = updatePublisher
                .receive(on: RunLoop.main)
                .sink ( receiveCompletion: { completion in  print ("Finished \(completion)") },
                        receiveValue: { [weak self]
                            (airports) in
                            var array = [StationIntegratedData]()
                            var mrkrs = [nearestPlaceMarker]()
                            for item in airports {
                                array.append(StationIntegratedData(instation: item.station))
                            }
                            self?.airports = array
                            //self?.placeMarkers = mrkrs
                            self?.placeMarkers = self?.createMarkers(array) ?? []
                            self?.span = self?.createSpan(for: self?.placeMarkers ?? []) ?? (0.2, 0.2)
                            self?.region.span.latitudeDelta = self?.span.0 ?? 0.7
                            self?.region.span.longitudeDelta = self?.span.1 ?? 0.7
                           // self?.region.span.longitudeDelta = self?.span.1 ?? 0.7
                            let center = self?.createCenter(for: self?.placeMarkers ?? []) ?? (0,0)
                            self?.region.center = CLLocationCoordinate2D(latitude: center.0, longitude: center.1)
                            print("Updating...")
                            
                        }
                )
        } else {
            self.airports = []
        }
    }

}




extension NearestViewModel {
    
    func createSpan(for marray: [nearestPlaceMarker]) -> (Double, Double) {
        
        let latitudes = marray.map { $0.latitude }
        let longitudes = marray.map { $0.longitude }
        let minLat = latitudes.min()
        let maxLat = latitudes.max()
        let minLon = longitudes.min()
        let maxLon = longitudes.max()
        let spanLat: Double
        let spanLon: Double
        if let maxX = maxLat, let minX = minLat, let maxY = maxLon, let minY = minLon {
            spanLat = (maxX - minX)*1.5
            spanLon = (maxY - minY)*1.5
        } else {
            spanLat = 0.2
            spanLon = 0.2
        }
        print("SPAN \(spanLat), \(spanLon)")
        return (spanLat, spanLon)
    }
}




extension NearestViewModel {
    
    func createCenter(for marray: [nearestPlaceMarker]) -> (Double, Double) {
        
        let latitudes = marray.map { $0.latitude }
        let longitudes = marray.map { $0.longitude }
        let minLat = latitudes.min()
        let maxLat = latitudes.max()
        let minLon = longitudes.min()
        let maxLon = longitudes.max()
        var centerLon: Double
        var centerLat: Double
        if let maxX = maxLat, let minX = minLat, let maxY = maxLon, let minY = minLon {
            centerLat = (maxX+minX)/2
            centerLon = (maxY+minY)/2
        } else {
            centerLat = 0
            centerLon = 0
        }
        return (centerLat, centerLon)
    }
}


extension NearestViewModel {
    
    func createMarker(_ airport: StationIntegratedData) -> nearestPlaceMarker {
        
        
            let arp = nearestPlaceMarker(
                icao: airport.integratedStation.icao,
                latitude: airport.integratedStation.latitude ?? 0.0,
                longitude: airport.integratedStation.longitude ?? 0.0,
                tint: airport.tintColor //?? .black
            )
            
        
       
        return arp
    }
}

extension NearestViewModel {
    
    func createMarkers(_ airports: [StationIntegratedData]) -> [nearestPlaceMarker] {
        var markers: [nearestPlaceMarker] = []
        airports.forEach() { arp in
            let airport = nearestPlaceMarker(
                icao: arp.integratedStation.icao,
                latitude: arp.integratedStation.latitude ?? 0.0,
                longitude: arp.integratedStation.longitude ?? 0.0,
                tint: arp.tintColor
            )
            print(airport.tint)
//            if let mrk = arp.marker {
//                markers.append(mrk)
//            }
            markers.append(airport)
        }
//        let myPlace = nearestPlaceMarker(
//            icao: "I'm here",
//            latitude: self.coordinate?.latitude ?? 0.0,
//            longitude: self.coordinate?.longitude ?? 0.0,
//            tint: .red
//        )
//        markers.append(myPlace)
        return markers
    }
}


struct nearestPlaceMarker: Identifiable {
    var id = UUID()
    var icao: String
    var latitude: Double
    var longitude: Double
    var tint: Color
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
