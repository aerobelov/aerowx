//
//  NearestMapView.swift
//  aeroWX
//
//  Created by Pavel Belov on 28.03.2021.
//

import Foundation
import SwiftUI
import MapKit

struct NearestMapView: View {

    @EnvironmentObject var nearestModel: NearestViewModel
   // @State var markers: [nearestPlaceMarkers]
    @Binding var region: MKCoordinateRegion
    @State var markers: [nearestPlaceMarker]
    
    var body: some View {
        Map(coordinateRegion: $region,
            showsUserLocation: true,
            annotationItems: nearestModel.placeMarkers) {
                
            place in
                MapAnnotation( coordinate: place.coordinate ) {
                    Text(place.icao)
                        .font(.subheadline)
                        .padding(3)
                        .background(place.tint)
                        .foregroundColor(Color(UIColor.label))
                        .cornerRadius(5)
                }
        }
    }
}
