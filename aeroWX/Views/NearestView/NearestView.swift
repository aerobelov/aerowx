//
//  SearchView.swift
//  aeroWX
//
//  Created by Pavel Belov on 02.02.2021.
//

import Foundation
import SwiftUI

struct NearestView: View {
    @StateObject var nearestModel = NearestViewModel()
    
    var body: some View {
        GeometryReader {
            metrics in
            VStack{
                NearestMapView(region: $nearestModel.region, markers:nearestModel.placeMarkers)
                    .frame(height:metrics.size.height * 0.4)
                NearestResultView()
            }
            .navigationTitle("Nearest airports")
            .environmentObject(nearestModel)
        }
        
    }
}
