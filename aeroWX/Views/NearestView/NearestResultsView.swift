//
//  SearchResultsView.swift
//  aeroWX
//
//  Created by Pavel Belov on 03.02.2021.
//

import Foundation
import SwiftUI
import Combine

struct NearestResultView: View {
   
    @EnvironmentObject var nearestModel: NearestViewModel
    
    var body: some View {
        List(nearestModel.airports, id: \.integratedStation.icao) { item in
            NavigationLink(destination: WXView(station: item)) {
                Cell(station: item)
            }
        }
        .listStyle(PlainListStyle())
    }
    
}
