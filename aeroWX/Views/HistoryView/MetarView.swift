//
//  HistoryView.swift
//  aeroWX
//
//  Created by Pavel Belov on 24.02.2021.
//

import Foundation
import SwiftUI

struct MetarView: View {
    
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(
        entity: Metar.entity(),
        sortDescriptors: [NSSortDescriptor(keyPath: \Metar.timestamp, ascending: false)]
    )
    var metars: FetchedResults<Metar>

    var body: some View {
        List {
            ForEach(metars, id: \.id) { metar in
                VStack (alignment: .leading) {
                    HStack {
                        Text(metar.station ?? "")
                            .fontWeight(.bold)
                        Spacer()
                        Text(short(date: metar.timestamp))
                            .fontWeight(.bold)
                    }
                    Text(metar.raw ?? "")
                        .font(.body)
                }
            }
            .onDelete(perform: { indexSet in
                delete(indexSet: indexSet)
            })
        }
        .listStyle(PlainListStyle())
    }
    
    func delete(indexSet: IndexSet) {
        for index in indexSet {
            let metar = metars[index]
            moc.delete(metar)
            try? moc.save()
        }
    }
    
    func short(date: Date?) -> String {
        if let dt = date {
            return dt.short()
        } else {
            return ""
        }
    }
}

