//
//  TafView.swift
//  aeroWX
//
//  Created by Pavel Belov on 26.02.2021.
//

import Foundation
import SwiftUI

struct TafView: View {
    
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(
        entity: Taf.entity(),
        sortDescriptors: [NSSortDescriptor(keyPath: \Taf.timestamp, ascending: false)]
    )
    var tafs: FetchedResults<Taf>

    var body: some View {
        List {
            ForEach(tafs, id: \.id) { taf in
                VStack (alignment: .leading) {
                    HStack {
                        Text(taf.station ?? "")
                            .fontWeight(.bold)
                        Spacer()
                        Text(taf.timestamp?.short() ?? "")
                            .fontWeight(.bold)
                    }
                    Text(taf.raw ?? "")
                        .font(.body)
                }
            }
            .onDelete(perform: { indexSet in
                delete(indexSet: indexSet)
            })
        }
        .listStyle(PlainListStyle())
    }
    
    func delete(indexSet: IndexSet) {
        for index in indexSet {
            let taf = tafs[index]
            moc.delete(taf)
            try? moc.save()
        }
    }
}

