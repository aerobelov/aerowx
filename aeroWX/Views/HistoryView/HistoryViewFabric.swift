//
//  HistoryViewFabric.swift
//  aeroWX
//
//  Created by Pavel Belov on 26.02.2021.
//

import Foundation
import SwiftUI

struct HistoryFabric {
   
    @ViewBuilder
    func selectorView (index: Int = 0) -> some View {
        switch index {
        case 0:
            MetarView()
        case 1:
            TafView()
        default:
            MetarView()
        }
    }
}
