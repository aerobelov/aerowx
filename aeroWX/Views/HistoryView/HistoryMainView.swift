//
//  HistoryMainView.swift
//  aeroWX
//
//  Created by Pavel Belov on 26.02.2021.
//

import Foundation
import SwiftUI

struct HistoryMainView: View {
    
    let fabric = HistoryFabric()
    @State var selectorIndex = 0
    @State var menu = ["METAR", "TAF"]
    
    var body: some View {
        VStack {
            Picker("Message", selection: $selectorIndex) {
                ForEach(0 ..< menu.count) { index in
                    Text(self.menu[index])
                }
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding()
            fabric.selectorView(index: selectorIndex)
            .listStyle(PlainListStyle())
        }
        .navigationTitle("Saved reports")
        
    }
    
}
