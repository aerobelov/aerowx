//
//  SearchResultView.swift
//  aeroWX
//
//  Created by Pavel Belov on 09.02.2021.
//

import Foundation
import SwiftUI

struct SearchResultView: View {
    @EnvironmentObject var searchModel: SearchViewModel
    
    var body: some View {
       
        List(self.searchModel.airports, id: \.integratedStation.icao) { item in
            NavigationLink(destination: WXView(station: item )) {
                Cell(station: item)
                }
            }
            .listStyle(PlainListStyle())
          
    }
}
