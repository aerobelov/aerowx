//
//  SearchView.swift
//  aeroWX
//
//  Created by Pavel Belov on 09.02.2021.
//

import Foundation
import SwiftUI
import CoreData

struct SearchView: View {
    
    @StateObject var searchViewModel = SearchViewModel()
    
    var body: some View {
            VStack {
                SearchBar(searchText: $searchViewModel.searchText)
                SearchResultView()
            }
            .environmentObject(searchViewModel)
            .navigationTitle("Search")
    }
    
}
