//
//  SearchViewModel.swift
//  aeroWX
//
//  Created by Pavel Belov on 09.02.2021.
//

import Foundation
import SwiftUI
import Combine

class SearchViewModel: ObservableObject {
    
    let icaoLimit = 4
    @Published var airports: [StationIntegratedData]
    @Published var searchText: String = "" {
        didSet {
            if searchText.count > icaoLimit {
                searchText = String(searchText.prefix(icaoLimit))
            }
        }
    }
    var disposable = Set<AnyCancellable>()
    var listenSubscription: AnyCancellable?
    
    
    func listen() {
        
        let manager = SearchManager()
        
        listenSubscription =
        self.$searchText
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .removeDuplicates()
            .map { $0.uppercased() }
            .map {
                manager.buildRequest(arp: $0)
            }
            .flatMap {
                manager.searchPublisher(request: $0!)
            }
            .receive(on: RunLoop.main)
            .sink { [weak self] value in
                if value.icao != "" {
                    self?.airports.append(StationIntegratedData(instation: value))
                }
            }
    }
    
    
    init() {
        airports = []
        listen()
    }
    
    init(text: String) {
        searchText = text
        airports = []
        listen()
    }
    
   
}
