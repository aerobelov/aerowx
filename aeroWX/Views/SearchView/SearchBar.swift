//
//  SearchBar.swift
//  aeroWX
//
//  Created by Pavel Belov on 09.02.2021.
//

import Foundation
import SwiftUI

struct SearchBar: View {
    
    @State private var isEditing = false
    @Binding var searchText: String 
    
    var body: some View {
        HStack {
            TextField("Search airport ICAO code", text: $searchText)
                .autocapitalization(.allCharacters)
                .padding(7)
                .padding(.horizontal, 15)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .padding(.horizontal, 10)
                .onTapGesture {
                    isEditing = true
                }
            if isEditing {
                Button(action: {
                    isEditing = false
                    searchText = ""
                })
                {
                    Text("Cancel")
                }
                .padding(.trailing, 10)
                .transition(.move(edge: .trailing))
                .animation(.default)
            }
        }
    }
}

struct SearchBar_Previews: PreviewProvider {
    @State static var placeholder1 = "Search"
    static var previews: some View {
        SearchBar(searchText:  $placeholder1)
            .previewDevice("iPhone 11")
    }
}
