//
//  FavoritesView.swift
//  aeroWX
//
//  Created by Pavel Belov on 27.02.2021.
//

import Foundation
import SwiftUI

struct FavoriteView: View {
   
    @FetchRequest(
        entity: Favorite.entity(),
        sortDescriptors: []
    )
    var favorites: FetchedResults<Favorite>
    
    var body: some View {
        List(favorites, id: \.icao) { item in
            NavigationLink(destination: FavoriteListView(icao: item.icao!)) {
                favoriteShortCell (forAirport: item)
            }
        }
        .navigationTitle("Favorites")
        .listStyle(PlainListStyle())
    }
    
    
}

extension FavoriteView {
    func favoriteShortCell (forAirport airport: Favorite) -> some View {
        
        VStack (alignment: .leading)  {
            HStack {
                Text(airport.name ?? "")
                    .font(.body)
                    .fontWeight(.bold)
                Spacer()
                Text(airport.country ?? "")
                    .font(.body)
                    .fontWeight(.bold)
            }
            .lineLimit(1)
            HStack {
                Text(airport.city ?? "")
                    .font(.subheadline)
                Spacer()
                Text("\(airport.icao ?? "" )/\(airport.iata ?? "")")
                    .font(.subheadline)
            }
            .lineLimit(1)
        }
    }
}
