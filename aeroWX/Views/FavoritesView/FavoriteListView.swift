//
//  WXView.swift
//  aeroWX
//
//  Created by Pavel Belov on 17.02.2021.
//

import Foundation
import SwiftUI
import Combine

struct FavoriteListView: View {
    
    @State var icao: String
    
    var body: some View {
        FavoriteWxDetailView(icao: icao)  
    }
}
