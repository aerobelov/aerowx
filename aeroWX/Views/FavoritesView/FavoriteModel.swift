//
//  SearchViewModel.swift
//  aeroWX
//
//  Created by Pavel Belov on 09.02.2021.
//

import Foundation
import SwiftUI
import Combine

class FavoriteModel: ObservableObject {
    
    @Published var station: Station?
    @Published var metar: MetarData?
    @Published var taf: TafData?
    @Published var imageName: String?
    @Published var handler = FavoritesHandler()
    var disposableBin = Set<AnyCancellable>()
    var icao: String?
    
    init() {
            //METAR SUBSCRIPTION
            $station
            .map { ($0?.icao ?? "") }
            .flatMap { [weak self] metar in
                (self?.getMetar(icao: metar))!
            }
            .receive(on: RunLoop.main)
            .sink { [weak self] metar in
                self?.metar = metar
            }
            .store(in: &disposableBin)
        
            //TAF SUBSCRIPTION
            $station
            .map { $0?.icao ?? "" }
            .flatMap { [weak self] taf in
                (self?.getTaf(icao: taf))!
            }
            .receive(on: RunLoop.main)
            .sink { [weak self] taf in
                self?.taf = taf
            }
            .store(in: &disposableBin)
        
            //CURRENT STATIONINTEGRATED INSTANCE IMAGE SUBSCRIPTION DEPENDS ON INSTANCE AIRPORT
            $station
                .map { ($0?.icao ?? "") }
                .receive(on: RunLoop.main)
                .sink { [weak self] station in
                    self?.imageName = self?.handler.image(icao: station)
                }
                .store(in: &disposableBin)
            
            //CURRENT STATIONINTEGRATED INSTANCE IMAGE SUBSCRIPTION DEPENDS ON FAVORITES LIST
        self.handler.$objects
                .sink { [weak self] objects in
                    if let strongself = self {
                        strongself.imageName = (strongself.handler.image(icao: strongself.station?.icao ?? "star"))
                    }

                }
                .store(in: &disposableBin)
            
    }
    
    deinit {
        disposableBin.removeAll()
    }
    
    func getMetar(icao: String) -> AnyPublisher<MetarData, Never> {
        let manager = MetarManager()
        let request = manager.buildRequest(icao: icao)
        guard request != nil else { return Just(MetarData()).eraseToAnyPublisher() }
        return manager.metarPublisher(request: request!)
    }
    
    func getTaf(icao: String) -> AnyPublisher<TafData, Never> {
        let manager = TAFManager()
        let request = manager.buildRequest(icao: icao)
        guard request != nil else { return Just(TafData()).eraseToAnyPublisher() }
        return manager.tafPublisher(request: request!)
    }

    //SUBSCRIBE TO STATION INFO AND ASSIGN SELF.AIRPORT TO INBOUND ICAO CODE STATION
    func subscribe(icao: String) {
        
        let manager = SearchManager()
        guard let request = manager.buildRequest(arp: icao) else { return }
       
            manager.searchPublisher(request: request)
            .receive(on: RunLoop.main)
            .sink { [weak self] value in
                if value.icao != "" {
                    self?.station = value
                }
            }
            .store(in: &disposableBin)
    }
    
    
   
}
