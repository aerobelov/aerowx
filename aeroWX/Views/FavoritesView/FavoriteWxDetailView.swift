//
//  WXDetailView.swift
//  aeroWX
//
//  Created by Pavel Belov on 06.03.2021.
//

import Foundation
import SwiftUI

struct FavoriteWxDetailView: View {
    
    @ObservedObject var model = FavoriteModel()
    var icao: String

    var body: some View {
        VStack {
            
            List {
                Section (header:
                            HStack {
                                Text("METAR")
                                Spacer()
                                RulesBadge(rules: model.metar?.flight_rules ?? "")
                            }
                        )
                {
                    Text(model.metar?.raw ?? "")
                }
                Section (header: Text("TAF")) {
                    Text(model.taf?.raw ?? "")
                }
                Section (header: Text("Airport information")) {
                    if let station = self.model.station {
                        StationInfoView(station: station)
                        HStack {
                            if let station = model.station {
                                Button(action:  { model.handler.toggle(airport: station) }, label: {
                                    Image(systemName: model.imageName ?? "star")
                                        .imageScale(.large)
                                })
                                Text("Favorite")
                            }
                           
                        }
                    }
                    
                }
            }
            .onAppear {self.model.subscribe(icao: self.icao)}
            
        }
        .navigationBarTitle("\(model.station?.icao ?? "")/\(model.station?.iata ?? "")")
        .navigationBarItems(trailing:
            SaveButton(metar:model.metar, taf: model.taf)
        )
       
        
    }
}
