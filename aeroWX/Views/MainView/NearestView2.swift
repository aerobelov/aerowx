//
//  SearchView.swift
//  aeroWX
//
//  Created by Pavel Belov on 02.02.2021.
//

import Foundation
import SwiftUI

struct NearestView: View {
    @ObservedObject var nearestViewModel: NearestViewModel
    
    var body: some View {
        VStack {
            NearestBar(searchText: $nearestViewModel.text)
            NearestResultView(resultList: $nearestViewModel.airports)
        }
    }
}
