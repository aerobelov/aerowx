//
//  TabView.swift
//  aeroWX
//
//  Created by Pavel Belov on 09.02.2021.
//

import Foundation
import SwiftUI

struct MainView: View {
    
    var body: some View {
            TabView {
                
                //NEAREST
                NavigationView {
                    NearestView()
                }
                .navigationViewStyle(StackNavigationViewStyle())
                .tabItem {
                    Label("Nearest", systemImage: "location")
                }
 
                //FAVORITES
                NavigationView {
                    FavoriteView()
                }
                .navigationViewStyle(StackNavigationViewStyle())
               
                .tabItem {
                    Label("Favorite", systemImage: "star")
                }
                
                //SEARCH
                NavigationView {
                    SearchView()
                }
                .navigationViewStyle(StackNavigationViewStyle())
                
                .tabItem {
                    Label("Search", systemImage: "magnifyingglass")
                }
                
                //HISTORY
                NavigationView {
                    HistoryMainView()
                }
                .navigationViewStyle(StackNavigationViewStyle())
                .tabItem {
                    Label("History", systemImage: "clock" )
                }
            }
    }
}
