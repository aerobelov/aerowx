//
//  StarButton.swift
//  aeroWX
//
//  Created by Pavel Belov on 11.03.2021.
//

import Foundation
import SwiftUI

struct StarButton: View {
    
    @EnvironmentObject var station: StationIntegratedData
    
    var body: some View {
        //if let station = self.station {
            HStack {
                Button(action:  { station.handler.toggle(airport: station.integratedStation) }, label: {
                    Image(systemName: station.imageName)
                        .imageScale(.large)
                })
                Text("Favorite")
            }
        //}
    }
}

