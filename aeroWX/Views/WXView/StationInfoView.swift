//
//  StationInfoView.swift
//  aeroWX
//
//  Created by Pavel Belov on 23.02.2021.
//

import Foundation
import SwiftUI

enum CoordType: String {
    case lat = "%02d"
    case lon = "%03d"
}

struct StationInfoView: View {
    
    var station: Station
    
    func format(coord: Double, type: CoordType) -> String {
        let degrees = Int(modf(coord).0)
        let minutes = Int(modf(modf(coord).1*60).0)
        let seconds = Int(round(modf(modf(coord).1*60).1*60))
        return ( "\(String(format: type.rawValue, degrees))\u{00B0} \(String(format: CoordType.lat.rawValue, minutes))' \(String(format: CoordType.lat.rawValue, seconds))\"")
    }
    
    var lat: String {
        guard station.latitude != nil else { return "" }
        switch station.latitude {
        case let x where x! >= 0:
            return "N \(format(coord: x!, type: .lat))"
            
        case let y where y! < 0:
            return "S \(format(coord: -y!, type: .lat))"
        default:
            return ""
        }
    }
    
    var lon: String {
        guard station.longitude != nil else { return "" }
        switch station.longitude {
        case let x where x! >= 0:
            return "E \(format(coord: x!, type: .lon))"
        case let y where y! < 0:
            return "W \(format(coord: -y!, type: .lon))"
        default:
            return ""
        }
    }
    
    var elevation: String {
        guard station.elevation_m != nil, station.elevation_ft != 0 else { return "" }
        return "Elevation \(String(station.elevation_m!)) m / \(String(station.elevation_ft!)) f"
    }
   
    var body: some View {
        VStack (alignment: .leading) {
            VStack (alignment: .leading) {
                Text(station.name ?? "")
                    .font(.title)
            }
            HStack (alignment: .center) {
                Text(station.city ?? "")
                Text("/")
                Text(station.country ?? "")
            }
            .font(.subheadline)
            Spacer()
            VStack (alignment: .leading) {
                Text(lat)
                Text(lon)
            }
            Spacer()
            VStack (alignment: .leading) {
                Text(elevation)
            }
            Spacer()
            VStack (alignment: .leading) {
                ForEach(station.runways ?? [], id: \.self) { runway in
                    HStack (alignment: .center) {
                        Text("\(runway.ident1)/\(runway.ident2)")
                        Spacer()
                        Text("\(runway.length_ft) ft, Width \(runway.width_ft) ft")
                    }
                }
            }
        }
        
    }
}
