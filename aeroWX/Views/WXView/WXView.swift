//
//  WXView.swift
//  aeroWX
//
//  Created by Pavel Belov on 17.02.2021.
//

import Foundation
import SwiftUI
import Combine

struct WXView: View {
    
    @StateObject var station: StationIntegratedData
   
   
    var body: some View {
            VStack {
                List {
                    Section (header:
                                HStack {
                                    Text("METAR")
                                    Spacer()
                                    RulesBadge(rules: station.metar?.flight_rules ?? "")
                                }
                            )
                    {
                        Text(station.metar?.sanitized ?? "")
                            .font(.body)
                    }
                    Section (header: Text("TAF")) {
                        Text(station.taf?.raw ?? "")
                            .font(.body)
                    }
                    Section (header: Text("Airport information")) {
                        StationInfoView(station: station.integratedStation)
                        StarButton()
                            .environmentObject(station)

                    }
                }
                
            }
            .navigationBarTitle("\(station.integratedStation.icao)/\(station.integratedStation.iata)")
            .navigationBarItems(trailing:
                SaveButton(metar: station.metar, taf: station.taf)
            )
        }
    
}
