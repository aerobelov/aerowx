//
//  SaveButton.swift
//  aeroWX
//
//  Created by Pavel Belov on 04.03.2021.
//

import Foundation
import SwiftUI

struct SaveButton: View {
    
    @Environment(\.managedObjectContext) var moc
    @State var isPresented: Bool = false
    
    var metar: MetarData?
    var taf: TafData?
    
    func save(in_metar: MetarData, in_taf: TafData, completion: ()->Void ) {
        
        if in_metar.raw.count > 0 {
            let metar = Metar(context: moc)
            metar.station = in_metar.station
            metar.flight_rules = in_metar.flight_rules
            metar.raw = in_metar.raw
            metar.timestamp = Date()
            metar.id = UUID()
        }
        if let tafraw = in_taf.raw, tafraw.count > 0 {
            let taf = Taf(context: moc)
            taf.station = in_taf.station
            taf.raw = tafraw
            taf.timestamp = Date()
            taf.id = UUID()
        }
        if moc.hasChanges {
            try? moc.save()
            completion()
        }
        
    }
    
    var body: some View {
        if let metar = self.metar, let taf = self.taf {
            Button(action: {
                save(in_metar: metar, in_taf: taf, completion: { self.isPresented = true })
            }) {
                Text("Save reports")
            }
            .alert(isPresented: self.$isPresented) {
                Alert(title: Text("Success"), message: Text("Reports saved"), dismissButton: .default(Text("Ok")))
            }
        }
    }
    
}
