//
//  TemperatureView.swift
//  aeroWX
//
//  Created by Pavel Belov on 23.02.2021.
//

import Foundation
import SwiftUI

struct TemperatureView: View {
    
    var data: MetarData
    
    var value: String {
        if var info = data.temperature?.repr,  info != "" {
            if info.prefix(1) == "M" {
                info = info.replacingOccurrences(of: "M", with: "-")
            } else {
                info = "+\(info)"
            }
            return "\(info)\u{00B0}\(data.units.temperature)"
        } else {
            return ""
        }
    }
    
    var body: some View {
        Text(value)
            .font(.subheadline)
    }

}
