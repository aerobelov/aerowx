//
//  Cell.swift
//  aeroWX
//
//  Created by Pavel Belov on 21.02.2021.
//

import Foundation
import SwiftUI

struct Cell: View {
    
    @ObservedObject var station: StationIntegratedData
    
    var body: some View {
        HStack {
            VStack (alignment: .leading)  {
                HStack {
                    Text(station.integratedStation.name!)
                        .font(.body)
                        .fontWeight(.bold)
                    Spacer()
                    Text(station.integratedStation.country!)
                        .font(.body)
                        .fontWeight(.bold)
                }
                .lineLimit(1)
                HStack {
                    Text(station.integratedStation.city!)
                        .font(.subheadline)
                    Spacer()
                    Text("\(station.integratedStation.icao)/\(station.integratedStation.iata)")
                        .font(.subheadline)
                }
                .lineLimit(1)
                HStack {
                    Image(systemName: station.imageName)
                    RulesBadge(rules:station.metar?.flight_rules ?? "")
                    Spacer()
                    Text(station.metar?.time.repr ?? "")
                        .font(.subheadline)
                    TemperatureView(data: station.metar ?? MetarData())
                }
            }
        }
    }
}



