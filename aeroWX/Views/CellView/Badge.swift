//
//  Badge.swift
//  aeroWX
//
//  Created by Pavel Belov on 22.02.2021.
//

import Foundation
import SwiftUI

struct RulesBadge: View {
    
    var rules: String
   
    var body: some View {
        Text(rules)
            .font(.subheadline)
            .fontWeight(.bold)
            .padding(1)
            .background(self.getColor())
            .foregroundColor(Color(UIColor.label))
            .cornerRadius(5)
    }
    
    func getColor() -> Color {
        switch self.rules {
        case "":
            return Color(UIColor.systemBackground)
        case "IFR":
            return .red
        case "VFR":
            return .green
        case "MVFR":
            return .blue
        case "LIFR":
            return .pink
        default:
            return Color(UIColor.systemBackground)
        }
    }
}
