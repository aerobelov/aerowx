//
//  Station.swift
//  aeroWX
//
//  Created by Pavel Belov on 18.02.2021.
//

import Foundation

struct Station: Decodable {
    
    var city: String?
    var country: String?
    var elevation_ft: Int?
    var elevation_m: Int?
    var iata: String
    var icao: String
    var latitude: Double?
    var longitude: Double?
    var name: String?
    //var note: [String: Data]?
    var reporting: Bool?
    var runways: [Runway]?
    var state: String?
    var type: String?
    
    init() {
        self.city = ""
        self.country = ""
        self.elevation_ft = 0
        self.elevation_m = 0
        self.iata = ""
        self.icao = ""
        self.latitude = 0.0
        self.longitude = 0.0
        self.name = ""
        //self.note = [String = Data]?
        self.reporting = false
        self.runways = [Runway()]
        self.state = ""
        self.type = ""
    }
    
    init(icao: String) {
        self.city = ""
        self.country = ""
        self.elevation_ft = 0
        self.elevation_m = 0
        self.iata = ""
        self.icao = icao
        self.latitude = 0.0
        self.longitude = 0.0
        self.name = ""
        //self.note = [String = Data]?
        self.reporting = false
        self.runways = [Runway()]
        self.state = ""
        self.type = ""
    }
}

struct Runway: Decodable, Hashable {
    
    var length_ft: Int
    var width_ft: Int
    var ident1: String
    var ident2: String
    
    init() {
        self.length_ft = 0
        self.width_ft = 0
        self.ident1 = ""
        self.ident2 = ""
    }
}


