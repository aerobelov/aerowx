//
//  TafDataModel.swift
//  aeroWX
//
//  Created by Pavel Belov on 17.02.2021.
//

import Foundation

struct TafData: Decodable {
    var raw: String?
    var time: Time?
    var station: String
    var forecast: [Forecast]?
    var units: Units?
    
    init() {
        self.station = ""
    }
   
    
}

struct Time: Decodable{
    var repr: String = ""
    var dt: String = ""
}

struct Forecast: Decodable {
    var flight_rules: String?
    var sanitized: String?
    var raw: String?
}


