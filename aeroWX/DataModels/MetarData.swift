//
//  MetarDataModel.swift
//  aeroWX
//
//  Created by Pavel Belov on 17.02.2021.
//

import Foundation
import SwiftUI

struct MetarData: Decodable {
    
    var meta: Meta
    var flight_rules: String
    var sanitized: String
    var raw: String
    var station: String
    var temperature: Temperature?
    var visibility: Visibility?
    var clouds: [Clouds]?
    var units: Units
    var time: Time
    var tint: Color {
        return getColor(for: flight_rules)
    }
    
    init() {
        self.meta = Meta()
        self.flight_rules = ""
        self.sanitized = ""
        self.raw = ""
        self.station = ""
        self.units = Units()
        self.temperature = Temperature()
        self.visibility = Visibility()
        self.clouds = [Clouds]()
        self.time = Time()
    }
    
    
}

extension MetarData {
    
    func getColor(for rules: String) -> Color {
        
        switch rules {
        case "":
            return Color(UIColor.systemBackground)
        case "IFR":
            return .red
        case "VFR":
            return .green
        case "MVFR":
            return .blue
        case "LIFR":
            return .pink
        default:
            return Color(UIColor.systemBackground)
        }
    }
}



struct Units: Decodable {
    var altimeter: String
    var altitude: String
    var temperature: String
    var visibility: String
    var wind_speed: String
    
    init() {
        self.altimeter = ""
        self.altitude = ""
        self.temperature = ""
        self.visibility = ""
        self.wind_speed = ""
    }
}

struct Meta: Decodable {
    var timestamp: String = ""
}

struct Temperature: Decodable {
    var repr: String = ""
    var value: Int = 0
}

struct Visibility: Decodable {
    var repr: String = ""
}

struct Clouds: Decodable {
    var repr: String = ""
    var type: String = ""
}



