//
//  StationIntegratedData.swift
//  aeroWX
//
//  Created by Pavel Belov on 21.02.2021.
//

import Foundation
import SwiftUI
import Combine

class StationIntegratedData: ObservableObject {
    
    @Published var integratedStation: Station
    @Published var metar: MetarData?
    @Published var taf: TafData?
    @Published var imageName: String
    @Published var handler = FavoritesHandler()
    @Published var marker: nearestPlaceMarker?
    
    var metarSubscription: AnyCancellable?
    var tafSubscription: AnyCancellable?
    var imageSubscription: AnyCancellable?
    var favoritesSubscription: AnyCancellable?
    var disposableBin = Set<AnyCancellable>()
    var tintColor: Color = Color(UIColor.systemBackground)
    
    
    func makeMarker() {
        print("MAKE MRK")
        self.marker = nearestPlaceMarker(
            icao: self.integratedStation.icao,
            latitude: self.integratedStation.latitude ?? 0.0,
            longitude: self.integratedStation.longitude ?? 0.0,
            tint: self.metar?.tint ?? .blue
        )
        print("\(self.marker?.tint)")
    }
    
    func update() {
        
        //METAR SUBSCRIPTION
        metarSubscription =
        $integratedStation
            .map { $0.icao }
            .flatMap { [weak self] metar in
                (self?.getMetar(icao: metar))!
            }
            .receive(on: RunLoop.main)
            .sink { [weak self] metar in
                print(metar.flight_rules)
                self?.metar = metar
                self?.makeMarker()
                //self?.tintColor = self?.getColor(for: metar.flight_rules) ?? Color(UIColor.systemBackground)
            }
            
        
        //TAF SUBSCRIPTION
        tafSubscription =
        $integratedStation
            .map { $0.icao }
            .flatMap { [weak self] taf in
                
                (self?.getTaf(icao: taf))!
            }
            .receive(on: RunLoop.main)
            .sink { [weak self] taf in
                self?.taf = taf
            }
        
        //CURRENT STATIONINTEGRATED INSTANCE IMAGE SUBSCRIPTION DEPENDS ON INSTANCE AIRPORT
        imageSubscription =
        $integratedStation
            .map { $0.icao }
            .receive(on: RunLoop.main)
            .sink { [weak self] station in
                self?.imageName = self?.handler.image(icao: station) ?? "star"
            }
            //.store(in: &disposableBin)
        
        //CURRENT STATIONINTEGRATED INSTANCE IMAGE SUBSCRIPTION DEPENDS ON FAVORITES LIST
        favoritesSubscription =
            handler.$objects
            .sink { [weak self] objects in
                if let strongself = self {
                    strongself.imageName = (strongself.handler.image(icao: strongself.integratedStation.icao))
                }

            }
            //.store(in: &disposableBin)
        
    }
    
    init() {
        integratedStation = Station()
        metar = MetarData()
        taf = TafData()
        imageName = "star"
        update()
    }
    
    init(instation: Station) {
        integratedStation = instation
        imageName = "star"
        update()
    }
    
    convenience init (icao: String) {
        self.init()
        integratedStation = Station(icao: icao)
    }
    
    
    func getMetar(icao: String) -> AnyPublisher<MetarData, Never> {
        let manager = MetarManager()
        let request = manager.buildRequest(icao: icao)
        guard request != nil else { return Just(MetarData()).eraseToAnyPublisher() }
        return manager.metarPublisher(request: request!)
    }
    func getTaf(icao: String) -> AnyPublisher<TafData, Never> {
        let manager = TAFManager()
        let request = manager.buildRequest(icao: icao)
        guard request != nil else { return Just(TafData()).eraseToAnyPublisher() }
        return manager.tafPublisher(request: request!)
    }
}

extension StationIntegratedData {
    
    func getColor(for rules: String) -> Color {
        
        switch rules {
        case "":
            return Color(UIColor.systemBackground)
        case "IFR":
            return .red
        case "VFR":
            return .green
        case "MVFR":
            return .blue
        case "LIFR":
            return .pink
        default:
            return Color(UIColor.systemBackground)
        }
    }
}
