//
//  StationDataModel.swift
//  aeroWX
//
//  Created by Pavel Belov on 18.02.2021.
//

import Foundation

struct StationData: Decodable {
    var station: Station
}

