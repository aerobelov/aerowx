//
//  StationIntegratedData.swift
//  aeroWX
//
//  Created by Pavel Belov on 21.02.2021.
//

import Foundation
import SwiftUI
import Combine

class ComplexStation: ObservableObject {
    
    var stationInfo: Station
    var metar: MetarData?
    var taf: TafData?
    var imageName: String
    var handler = FavoritesHandler.shared
    
    var metarSubscription: AnyCancellable?
    var tafSubscription: AnyCancellable?
    var imageSubscription: AnyCancellable?
    var favoritesSubscription: AnyCancellable?
    var disposableBin = Set<AnyCancellable>()
    
    func update() {
        
        //METAR SUBSCRIPTION
        metarSubscription =
        $integratedStation
            .map { $0.icao }
            .flatMap { [weak self] metar in
                (self?.getMetar(icao: metar))!
            }
            .receive(on: RunLoop.main)
//            .assign(to: \.metar, on: self)
            .sink { [weak self] metar in
                self?.metar = metar
                //print(self?.metar)
            }
            //.store(in: &disposableBin)
        
        //TAF SUBSCRIPTION
        tafSubscription =
        $integratedStation
            .map { $0.icao }
            .flatMap { [weak self] taf in
                
                (self?.getTaf(icao: taf))!
            }
            .receive(on: RunLoop.main)
//            .assign(to: \.taf, on: self)
            .sink { [weak self] taf in
                self?.taf = taf
            }
            //.store(in: &disposableBin)
        
        //CURRENT STATIONINTEGRATED INSTANCE IMAGE SUBSCRIPTION DEPENDS ON INSTANCE AIRPORT
        imageSubscription =
        $integratedStation
            .map { $0.icao }
            .receive(on: RunLoop.main)
            .sink { [weak self] station in
                self?.imageName = self?.handler.image(icao: station) ?? "star"
            }
            //.store(in: &disposableBin)
        
        //CURRENT STATIONINTEGRATED INSTANCE IMAGE SUBSCRIPTION DEPENDS ON FAVORITES LIST
        favoritesSubscription =
            FavoritesHandler.shared.$objects
            .sink { [weak self] objects in
                if let strongself = self {
                    strongself.imageName = (FavoritesHandler.shared.image(icao: strongself.integratedStation.icao))
                }

            }
            //.store(in: &disposableBin)
        
    }
    
    init() {
        integratedStation = Station()
        metar = MetarData()
        taf = TafData()
        imageName = "star"
        update()
    }
    
    init(instation: Station) {
        print("INSTATION INIT \(instation.icao)")
        integratedStation = instation
        //metar = MetarData()
        //taf = TafData()
        imageName = "star"
        update()
    }
    
    convenience init (icao: String) {
        self.init()
        print("CONV INIT \(icao)")
        integratedStation = Station(icao: icao)
    }
    
    deinit {
        print("STATION INTEGR DEINIT")
    }
    
    func getMetar(icao: String) -> AnyPublisher<MetarData, Never> {
        let manager = MetarManager()
        let request = manager.buildRequest(icao: icao)
        guard request != nil else { return Just(MetarData()).eraseToAnyPublisher() }
        return manager.metarPublisher(request: request!)
    }
    func getTaf(icao: String) -> AnyPublisher<TafData, Never> {
        let manager = TAFManager()
        let request = manager.buildRequest(icao: icao)
        guard request != nil else { return Just(TafData()).eraseToAnyPublisher() }
        return manager.tafPublisher(request: request!)
    }
}
