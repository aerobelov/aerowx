//
//  FavoritesHandler.swift
//  aeroWX
//
//  Created by Pavel Belov on 27.02.2021.
//

import Foundation
import CoreData
import SwiftUI

class FavoritesHandler: ObservableObject {
    var moc = PersistenceController.shared.container.viewContext
    var fetchAll: NSFetchRequest<Favorite> = Favorite.fetchRequest()
    @Published var objects: [Favorite]
    
    init() {
        self.objects = []
        update()
    }
    
    deinit {
//        print("FAV DEINIT")
    }
    
    func update() {
        let fetchRequest: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        do {
            try objects = moc.fetch(fetchRequest)
        } catch {
            objects = []
        }
    }
    
    
    func image(icao: String) -> String {
        let fetchRequest: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "icao == %@", icao)
        var objects: [Favorite]?
        do {
            objects = try moc.fetch(fetchRequest)
        } catch {
            return "star"
        }
        if objects!.count == 0 {
            return "star"
        } else {
           return "star.fill"
        }
    }
    
    
    func toggle(airport: Station) {
        let fetchRequest: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "icao == %@", airport.icao)
        var objects: [Favorite]?
        do {
            objects = try moc.fetch(fetchRequest)
        } catch {
//            print ("Save error")
        }
        guard objects != nil else { return }
        
        if objects!.count > 0 {
            let object = objects?.last
            moc.delete(object!)
            self.update()
        } else if objects!.count == 0 {
            let object = Favorite(context: moc)
            object.icao = airport.icao
            object.iata = airport.iata
            object.name = airport.name
            object.city = airport.city
            object.country = airport.country
            object.id = UUID()
            try? moc.save()
            self.update()
        }
    }
    
    
}
